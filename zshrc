export ZSH="$HOME/.oh-my-zsh"
export LC_ALL=en_US.UTF-8 export LANG=en_US.UTF-8
ZSH_THEME="af-magic"
plugins=(git)
source $ZSH/oh-my-zsh.sh

##### SSH #####
eval $(ssh-agent -s)
ssh-add $HOME/.ssh/id_rsa
ssh-add $HOME/.ssh/id_ed25519

##### Kube ps1 conf #####
source "/usr/local/opt/kube-ps1/share/kube-ps1.sh"
RPROMPT='$(kube_ps1)'

##### ALIASES #####
alias k="kubectl"
alias kctx="kubectx"
alias kns="kubens"
alias auth-vault='unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY VAULT_TOKEN VAULT_ADDR; export VAULT_ADDR=https://vault.tools.thefork.tech; export VAULT_TOKEN=$(vault login -method=aws -token-only role=trf-iamrole_vault-config-auth-iam-core); echo "vault token: $VAULT_TOKEN"'
alias update-kubeconfig='export CURR_DIR=$(pwd); export KUBE_TMP_DIR=$(mktemp -d); cd $KUBE_TMP_DIR; aws s3 cp s3://trf-config-kubernetes/ . --recursive; KUBECONFIG=$(ls -1 aws-*.yaml | xargs | tr " " ":") kubectl config view --flatten > ~/.kube/config; cd $CURR_DIR'
alias kiali-token='echo $(kubectl -n istio-system get secret $(kubectl -n istio-system get secret | grep -i kiali-token | awk '\''{print $1}'\'') -o jsonpath='\''{.data.token}'\'' | base64 --decode)'
alias gpp='git checkout master; git pull --rebase; git fetch --prune; gbp; gbb'
alias gbp='git branch --merged | grep -v "\*" | grep -v "master" | grep -v "develop" | grep -v "staging" | xargs -n 1 git branch -d'
alias gbb='git branch -vv | grep '\'': gone]'\''|  grep -v "\*" | awk '\''{ print $1;  }'\'' | xargs -r git branch -D'
function renew_aws () {
  CURRENT_KEY="$(aws configure get aws_access_key_id --profile default)"

  echo "current_key = $CURRENT_KEY"
  RESPONSE="$(aws iam create-access-key --output json | jq .AccessKey)"
  NEW_ACCESS_KEY_ID="$(echo $RESPONSE | jq -r '.AccessKeyId')"
  NEW_SECRET_ACCESS_KEY="$(echo $RESPONSE | jq -r '.SecretAccessKey')"
  echo "new access_key = $NEW_ACCESS_KEY_ID"

  echo "deleting current access key"
  aws iam delete-access-key --access-key-id $CURRENT_KEY

  echo "setting up new keys"
  aws configure set aws_access_key_id $NEW_ACCESS_KEY_ID --profile default
  aws configure set aws_secret_access_key $NEW_SECRET_ACCESS_KEY --profile default
}
##### PATH #####
export PATH="/usr/local/bin/vim:$PATH"
if [ "$TMUX" = "" ]; then tmux; fi
eval $(thefuck --alias)
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
