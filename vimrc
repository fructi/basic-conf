"----------Plugins----------"
" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
" Theme plugins
Plug 'tomasiser/vim-code-dark'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Syntax n shit
Plug 'Valloric/YouCompleteMe'
Plug 'scrooloose/syntastic'

" Insert or delete brackets, parens, quotes in pair
Plug 'jiangmiao/auto-pairs'

" Comment smartly
Plug 'preservim/nerdcommenter'

" On-demand loading
Plug 'airblade/vim-gitgutter', {'on':'GitGutterEnable'}
Plug 'junegunn/goyo.vim', {'on':'Goyo'}
Plug 'preservim/nerdtree', {'on':['NERDTree','NERDTreeToggle','NERDTreeFind','NERDTreeFocus']}
Plug 'xuyuanp/nerdtree-git-plugin', {'on':['NERDTree','NERDTreeToggle','NERDTreeFind','NERDTreeFocus']}
Plug 'ryanoasis/vim-devicons', {'on':['NERDTree','NERDTreeToggle','NERDTreeFind','NERDTreeFocus']}
Plug 'tiagofumo/vim-nerdtree-syntax-highlight', {'on':['NERDTree','NERDTreeToggle','NERDTreeFind','NERDTreeFocus']}
Plug 'ekalinin/Dockerfile.vim', {'for':'dockerfile'}
Plug 'fatih/vim-go', {'for':'go'}
Plug 'plasticboy/vim-markdown', {'for':['markdown','md']}
Plug 'chr4/nginx.vim', {'for':'nginx'}
Plug 'hashivim/vim-terraform', {'for':['hcl','terraform','tf']}
Plug 'stephpy/vim-yaml', {'for':['yaml', 'yml']}
Plug 'towolf/vim-helm', {'for': ['gotmpl']}
call plug#end()

" Theme
let g:airline_theme = 'codedark'
colorscheme codedark

" Some all around conf
retab
syntax on
filetype on
filetype plugin indent on
set background=dark
set number
set nobackup
set nocompatible
set cursorline
set encoding=UTF-8
set ruler
set showmatch
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autowriteall
set autoindent
set visualbell
set smartindent
set fileformat=unix
set backspace=indent,eol,start
set complete=.,w,b,u
set updatetime=100
set hlsearch
set incsearch

" Add warnings message to the statusline
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Plugin configurations
let g:goyo_width='100%'
let g:goyo_height='100%'
let g:vim_markdown_folding_disabled=1
let g:hcl_align=1
let g:terraform_align=1
let g:terraform_fmt_on_save=1
let g:NERDTreeMinimalUI=1
let g:NERDTreeDirArrows=0
let g:NERDTreeShowHidden=0
let g:NERDTreeAutoDeleteBuffer = 1
let g:NERDTreeRespectWildIgnore=1
let g:NERDTreeIgnore=['\.git$','\.svn$','\.hg$','\.DS_Store$','\.devcontainer$','\.vscode$','__pycache__$','\.terraform$','\.terraform.lock.hcl$','\.terragrunt-cache$','\.pyc$']
let g:WebDevIconsOS='Darwin'
let g:webdevicons_enable=1
let g:webdevicons_enable_nerdtree=1
let g:WebDevIconsUnicodeDecorateFileNodes=1
let g:webdevicons_conceal_nerdtree_brackets=1
let g:WebDevIconsUnicodeDecorateFolderNodes=1

" Syntastic config
let g:syntastic_auto_loc_list = 1
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_jump = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 5
let g:syntastic_enable_signs=1
let g:syntastic_error_symbol='✘' " '✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_style_error_symbol='e' " '✗'
let g:syntastic_style_warning_symbol='w'
let g:syntastic_aggregate_errors = 1
" python syntastic config
let g:syntastic_python_python_exec = '/usr/bin/python2.7'
let g:syntastic_python_checkers =
\ ['pylint', 'flake8', 'python', 'black']
let g:syntastic_enable_python_checker = 1

let mapleader = ','
" Toggle IDE mode in VIM
nmap <C-i> :GitGutterEnable<cr>:NERDTreeFind<cr>
" Toggle GitGutter
nmap <C-g> :GitGutterEnable<cr>
" Toggle Fullscreen mode
nmap <C-f> :Goyo<cr>
" Toggle Terminal below current tab
nmap <C-d> :below terminal<cr>
" make NERDTree easier
nmap <C-t> :NERDTreeToggle<cr>
nmap <C-n> :NERDTreeFind<cr>

" Start NERDTree when Vim starts with a directory argument.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>"

" Automatically source the Vimrc file on save.
augroup autosourcing
    autocmd!
    autocmd BufWritePost .vimrc source %
augroup END
